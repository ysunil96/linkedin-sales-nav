console.log("Main js Here")

let ACCOUNT_KEY = ""
let EMAIL_ID = ""
const HOST = "https://prospectss.com/app"
const VERIFICATION_ENDPOINT = "/api/extensions/authenticate-account-key/"







let isAuthenticated = false
window.addEventListener('load', async function () {
  chrome.runtime.onMessage.addListener(messageHandler);
  selectPage();
  
    
})

async function selectPage() {
  const tabs = await chrome.tabs.query({ 'active': true, 'lastFocusedWindow': true, 'currentWindow': true })
  const url = tabs[0].url
  const salesregex = /^https:\/\/www[.]linkedin[.]com\/sales/

  if (!getKey()) {
    loadAuthenticate()
    return
  }

  if (!salesregex.test(url)) {
    loadsalespage()
    return
  }

  const data = getKey()
  EMAIL_ID = data.email_id
  ACCOUNT_KEY = data.account_key
  isAuthenticated = true
  loadLoading()
  getTeamDetails()
  loadSearch()
  searchPage()


 
}

async function searchPage() {
  const tabs = await chrome.tabs.query({ 'active': true, 'lastFocusedWindow': true, 'currentWindow': true })
  const url = tabs[0].url
  const searchregex = /^https:\/\/www[.]linkedin[.]com\/sales\/search/


  if (searchregex.test(url)) {
    loadStart()
    return
  }
  else{
    loadSearch()
  }



}
  


function loadLoading() {
  let content = getHeaderMarkup()
  content += `<main class="loading">
<div class="container flex align-center justify-center">
  <div>
    <img src="/extension-icon/loader.svg" alt="loading">
  </div>
  <h1 class="text-heading text-center m-m m-nb">Fetching details...</h1>
</div>
</main>`
  content += getLoggedInFooterMarkup()
  document.body.innerHTML = content
  document.querySelector('#logout').addEventListener('click', logout)
}

function loadSuccess() {
  let content = getHeaderMarkup()
  content += `<main class="success">
    <div class="container flex align-center justify-center">
      <div>
        <img src="/extension-icon/right.svg" alt="authentication successful">
      </div>
      <h1 class="text-heading text-center m-m m-nb">Successful Authentication</h1>
      <p class="text-para text-center m-s">Switch to a website to continue</p>
    </div>
  </main>`
  document.body.innerHTML = content
  getTeamDetails()
}

function loadWrong() {
  let content = getHeaderMarkup()
  content += `<main class="wrong">
    <div class="container flex align-center justify-center">
      <div>
        <img src="/extension-icon/wrong.svg" alt="authentication successful">
      </div>
      <h1 class="text-heading text-center m-m m-nb">Unsuccessful Authentication</h1>
      <p class="text-para text-center m-s">Wrong Credentials...</p>
    </div>
    <div class="input-group">
      <button id="wrong-try-again" class="button-primary flex justify-between align-center">
        <span>Try Again</span>
        <span>
          <img src="/extension-icon/chevron-right.svg" alt="chevron right">
        </span>
      </button>
    </div>
  </main>`
  document.body.innerHTML = content
  document.querySelector('#wrong-try-again').addEventListener('click', loadAuthenticate)
}

function loadReload() {
  let content = getHeaderMarkup()
  content += `<main class="reload">
    <div class="container flex align-center justify-center">
      <div>
        <img src="/extension-icon/reload.svg" alt="reload the page">
      </div>
      <h1 class="text-heading text-center m-m m-nb">Couldn't fetch details…</h1>
      <p class="text-para text-center m-s">Refresh the page to continue</p>
    </div>
  </main>`
  content += getLoggedInFooterMarkup()
  document.body.innerHTML = content
  document.querySelector('#logout').addEventListener('click', logout)
}

function loadAuthenticate() {
  let content = getHeaderMarkup()
  content += `<main>
    <div class="container">
      <h1 class="m-s text-center text-heading">Welcome</h1>
      <p class="m-m text-center text-para">To continue you need to first authenticate yourself</p>
      <form id="auth-form" autocomplete="on">

        <div class="input-group">
          <input type="text" id="account_key" minlength="19" maxlength="19" placeholder="Enter Account Key" required />
        </div>
        <div class="input-group">
          <input type="email" id="email_id" placeholder="Enter your email" required/>
        </div>
        <div class="input-group">
          <button id="authenticate_button" type="submit" class="button-primary flex justify-between align-center">
            <span>Authenticate</span>
            <span>
              <img src="/extension-icon/chevron-right.svg" alt="chevron right">
            </span>
          </button>
        </div>
      </form>
    </div>
  </main>
  <footer class="authenticate-footer flex justify-center align-center">
    <div>
      <div class="m-s help text-center">
        <a href="https://prospectss.com/app/profile/" target="_blank" rel="noopener noreferrer">Where I can find the account key?</a>
      </div>
      <div class="m-s help text-center">
        <a href="https://prospectss.com/contact/" target="_blank" rel="noopener noreferrer">Help</a>
      </div>
    </div>
  </footer>`
  document.body.innerHTML = content
  document.querySelector("#auth-form").addEventListener('submit', authenticate)
}


function loadSearch(){
  let content = getHeaderMarkup()
  content += `<main class="start">
  <div class="container">
      <div class="flex-col align-center m-m ">
          <div class="slack-image-container"><img src="./images/lsn.svg" alt="slack team icon"></div>
          <div class="slack-title fs-xl fw-b m-s"><p>Please Search inorder <br> to Scrape Profiles</div>
          
      </div>
      </main>`
      content += getLoggedInFooterMarkup()
      document.body.innerHTML = content
      document.querySelector('#logout').addEventListener('click', logout)

}


function loadStart() {
  let content = getHeaderMarkup()
  content += `<main class="start">
        <div class="container">
            <div class="flex-col align-center m-m ">
                <div class="slack-image-container"><h3>Search Query : </h3><p id="searchWord"></p></div>
                <div class="dp"><p>You will be able to fetch these Data Points</p></div>
                <div class="data-points"><button>name</button><button>first_name</button><button>last_name</button><button>company</button><button>designation</button><button>location</button><button>tenure_at_company</button><button>premium_member</button><button>connection_degree</button><button>industry</button><button>profile_viewed</button><button>company_linkedin_url</button><button>profile_pic</button><button>linkedin_profile_link</button><button>about</button><button>experiences</button></div>
              
            </div>
            <div class="flex-col align-center m-m">
                <div class="number-scrape"><p>How many Members to Scrape? <span>i</span></p><br><input type="text" id="profile-count"class="scrape-input" name="scrape" placeholder="No. of Profiles" required></div>
            </div>
            <div class="flex-col align-center justify-center m-m input-group processing"><button id="start-scraping" class="button-primary fw-b fs-xl m-m">Start Scraping</button></div>
            <p class="warning text-center m-s fw-b fs-m none">Please wait. Do not click anywhere or change the tab.</p>
        </div>
    </main>`
  content += getLoggedInFooterMarkup()
  document.body.innerHTML = content
  chrome.runtime.onMessage.addListener(function(request, sender, response){
    document.getElementById('searchWord').innerHTML = request.searchWord;

  })
  document.querySelector('#logout').addEventListener('click', logout)
  document.getElementById('start-scraping').addEventListener('click', function (){
      
  })
  document.querySelector('#start-scraping').addEventListener('click',  () => {
    /* const processing = document.querySelector('.processing')
    processing.innerHTML = '<p class="progress fw-b fs-xl text-center m-s">Scraping in progress...</p>'
    document.querySelector('.warning').classList.remove('none') */
    scrapeStart();
    scrapeClickOn();
    setTimeout(() => {
      scrapeComplete();
    }, 1000);
    
  } )
}


function scrapeStart() {
  let content = getHeaderMarkup()
  content += `<main class="start">
        <div class="container">
            <div class="flex-col align-center m-m ">
                <div class="slack-image-container"><h3>Search Query : </h3><p id="searchWord"></p></div>
                <div class="dp flex-col align-center justify-center m-m"><div class="number-scraped"><h1>1</h1><span><h1>/</h1></span> <h1>2000 </h1></div>
                <br>
                <p class="ms">MEMBERS SCRAPED</p>
                
                </div>
              
            </div>
            <div class="flex-col align-center m-m">
            <br>
            <p class="progress fw-b fs-xl text-center m-s">Scraping in progress...</p>
            <p class="warning text-center m-s fw-b fs-m none">Please wait. Do not click anywhere or change the tab.</p>
            </div>
        </div>
    </main>`
  content += getLoggedInFooterMarkup()
  document.body.innerHTML = content
  chrome.runtime.onMessage.addListener(function(request, sender, response){
    document.getElementById('searchWord').innerHTML = request.searchWord;

  })
  
  document.querySelector('#logout').addEventListener('click', logout)
  
  
}


function scrapeComplete() {
  let content = getHeaderMarkup()
  content += `<main class="start">
        <div class="container">
            <div class="flex-col align-center m-m ">
                <div class="slack-image-container"><h3>Search Query : </h3><p id="searchWord"></p></div>
                <div class="dp flex-col align-center justify-center m-m"><div class="number-scraped"><h1 id="number">2000</h1></div>
                <br>
                <p class="ms">TOTAL MEMBERS SCRAPED</p>
                
                </div>
              
            </div>
            <div class="flex-col align-center m-m">
            <br>
            <div class="flex-col align-center justify-center m-m input-group"><button id="view-data" >View Data</button></div>
            </div>
        </div>
    </main>`
  content += getLoggedInFooterMarkup()
  document.body.innerHTML = content
  document.querySelector('#logout').addEventListener('click', logout)
  document.getElementById("view-data").addEventListener('click', function() {
    //exportCSVFile();
  })
 
  
}



function loadsalespage() {
  let content = getHeaderMarkup()
  content += `<main class="slackPage">
        <div class="container flex justify-center align-center">
        <p class="fs-xl fw-b">Please Switch to LinkedIn Sales Navigator Search Tab</p>
        </div>
    </main>
`
  content += getLoggedInFooterMarkup()
  document.body.innerHTML = content
  document.querySelector('#logout').addEventListener('click', logout)

}


function getHeaderMarkup() {
  return `<header class="flex justify-center align-center">
        <div class="logo">
            <img class="logo-image" src="/images/logo-color.png" alt="logo" />
        </div>
        <div id="variable"> </div>
    </header>`
}

function getLoggedInFooterMarkup() {
  return ` <footer class="footer-loggedIn">
        <div class="footer-toolkit">
            <a href="https://prospectss.com/" rel="noopener noreferrer" target="_blank"
                class="button-marketing button-primary flex align-center justify-center">
                <img src="/extension-icon/gem.svg" class="gem-icon" alt="get full functionality">
                <span>Get Full Marketing Toolkit</span>
            </a>
            <a id="logout"><img src="/extension-icon/logout.svg" alt="logout"></a>
            <a href="https://prospectss.tawk.help/" target="_blank" rel="noopener noreferrer"><img
                    src="/extension-icon/help.svg" alt="help"></a>
        </div>
    </footer>`
}



async function messageHandler(response) {
  if (response.type === "userDetails") {
    if (response.status) {
      allUserData.push(...response.result)
      //doing every time so if giving functionality to stop the scrape in future
      //otherwise allUserData.length will be same
      teamDetails.total_members_scraped += response.numUsers
      if (response.marker) {
        updateUserinDOM()
        await sleep(100)
        getUserDetails(response.marker)
      }
      else {
        scrapeComplete()
      }
    }
    else {
      console.error("user details not found")
      loadReload()
    }
  }
  if (response.type === "teamDetails") {
    if (response.status) {
      teamDetails = response.result
      teamDetails.total_members_scraped = 0
      loadStart()
    } else {
      loadReload()
    }
  }
  if (response.type === "load") {
    getTeamDetails()
  }
}

function sleep(time) { return new Promise(resolve => setTimeout(resolve, time)) }

function authenticate(event) {
  event.preventDefault()
  event.stopPropagation()
  const account_key = event.srcElement[0].value
  const email_id = event.srcElement[1].value
  loadLoading()
  fetch(HOST + VERIFICATION_ENDPOINT, {
    method: 'POST',
    headers: {
      "X-AUTH-TOKEN": "arun@arunpassword",
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body: `email_id=${encodeURIComponent(email_id)}&account_key=${encodeURIComponent(account_key)}`
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.verified === true) {
        // loadSuccess()
        isAuthenticated = true
        setKey({ email_id, account_key })
        selectPage()
      }
      else loadWrong()
    }).catch((err) => {
      loadReload()
    })
}


// //key and email in local storage

function setKey(data) {
  localStorage.setItem("data", JSON.stringify(data))
}

function getKey() {
  return JSON.parse(localStorage.getItem("data"))
}
function clearKey() {
  localStorage.clear()
}

function logout() {
  clearKey()
  loadAuthenticate()
}

async function getTeamDetails() {
  try {
    const tabs = await chrome.tabs.query({ active: true, currentWindow: true })
    chrome.tabs.sendMessage(tabs[0].id, { teamDetails: true }, ack => {
      if (chrome.runtime.lastError) chrome.tabs.reload(tabs[0].id)
    })
  }
  catch (error) {
    loadReload()
  }
}

async function getUserDetails(marker) {
  try {
    const tabs = await chrome.tabs.query({ active: true, currentWindow: true })
    chrome.tabs.sendMessage(tabs[0].id, { userDetails: true, marker: marker }, ack => {
      if (chrome.runtime.lastError) chrome.tabs.reload(tabs[0].id)
    })
  }
  catch (error) {
    loadReload()
  }
}

function scrapeClickOn() {

  chrome.runtime.onMessage.addListener(function(request, sender, response){

    console.log(request)

    var Data = {
      "search_keyword": "",
      "total_members_scraped": "",
      "members": []
  };



  var start = 0;
  var count = 100;


  var numberOfProfile = window.prompt("Enter No. of profiles to fetch");
  var numberOfPages, pageroundof, rem, pages;
  if (numberOfProfile <= 100) {
      count = numberOfProfile;
      pages = 1;
  }
  else {
      numberOfPages = numberOfProfile / 100;
      rem = numberOfPages % 1;
      rem = Number(rem.toFixed(2));
      pageroundof = numberOfPages - rem;

      if (rem != 0) {
          pages = pageroundof + 1;
      }
      else {
          pages = pageroundof;
      }


  }



  //Data.total_members_scraped = numberOfProfile;


  // Getting Search Keyword
  //var keyword = /keyword\w*=([^;]+)/i.test(document.location) ? RegExp.$1 : false;
  //var key_word = keyword.replaceAll('%', "").split('&')[0];
  

  Data.search_keyword = request.searchWord;


  const extra = rem * 100;

 


  const startScrape = async () => {



      // Getting Search ID
      // var search_id = /Sear\w*ID=([^;]+)/i.test(document.location) ? RegExp.$1 : false;
      // var searchID = search_id.substring(0,10);
      ////console.log(searchID);

      // Getting SessionID

      //var sess_id = /SESS\w*ID=([^;]+)/i.test(document.location) ? RegExp.$1 : false;
      //var session_id = sess_id.replaceAll('%',"");
      ////console.log(session_id);
     


      // Getting CSRF Token from chrome
      //var csrf_token = /SESS\w*ID=([^;]+)/i.test(document.cookie) ? RegExp.$1 : false;
      //var csrf = csrf_token.split('"')[1];
     
      // Getting Cookie
      //var cookie = document.cookie.split(';')[0];
      

      // Fetching Response From Server
      var myHeaders = new Headers();      // Creating Headers
      myHeaders.append("cookie", request.cookie); // Adding Browser Cookie to Header
      myHeaders.append("csrf-token", request.csrf); // Adding CSRF Token to Header
      myHeaders.append("x-restli-protocol-version", "2.0.0");

      // Requesting with Method, headers
      var requestOptions = {
          method: 'GET',
          headers: myHeaders,
          redirect: 'follow'
      };



      // API Url for LinkedIn Sales Search
       fetch(`https://www.linkedin.com/sales-api/salesApiPeopleSearch?q=peopleSearchQuery&start=${start}&count=${count}&query=(doFetchHeroCard:true,keywords:${request.searchWord},recentSearchParam:(doLogHistory:true),spellCorrectionEnabled:true,spotlightParam:(selectedType:ALL),doFetchFilters:true,doFetchHits:true,doFetchSpotlights:true)&decorationId=com.linkedin.sales.deco.desktop.search.DecoratedPeopleSearchHitResult-10`, requestOptions)
          .then(response => response.text())
          .then((result) => {
              let data = JSON.parse(result); // parsing the response into JSON Object
              let len = data.elements.length;

              /*
               console.log(data.paging.start);
              console.log(data.paging.count);
              console.log(data.paging.total);
             */

              for (let i = 0; i < len - 1; i++) {


                  let past_role = "";
                  let industry = "";
                  let profilePictureUrl = "";
                  let profileUrn = data.elements[i].entityUrn.split('(')[1].replaceAll(")", "");
                  let profile_url = "https://www.linkedin.com/sales/people/" + profileUrn;
                  let conn_degree = "";
                  let premium = "";
                  let company_url = "";
                  let companyName = "";
                  let tenure = "";
                  let title = "";
                  let summary = "";
                  let visited = "";
                  let companyNo = "";
                  let company_domain_url = "";
                  let  firstName="";
                  let lastName = "";
              


                  try {
                      if (data.elements[i].currentPositions[0].companyUrn.split('y:')[1] === undefined) {
                          companyNo = "1337";
                      }
                      else {
                          companyNo = data.elements[i].currentPositions[0].companyUrn.split('y:')[1];
                      }
                  } catch (error) { }


                  try {
                      if (data.elements[i].currentPositions[0].companyUrn === undefined) {
                          company_url = "";

                      }
                      else {
                          company_url = "https://www.linkedin.com/sales/company/" + data.elements[i].currentPositions[0].companyUrn.split('y:')[1];
                      }

                  } catch (error) {
                  }


                  try {
                      if (data.elements[i].viewed === undefined) {
                          visited = "";
                      }
                      else {
                          visited = data.elements[i].viewed;
                      }
                  } catch (error) { }



                  try {
                      if (data.elements[i].summary === undefined) {
                          summary = "";
                      }
                      else {
                          summary = data.elements[i].summary.replaceAll(/[\"]/g, "").replaceAll('\n', "");
                      }
                  } catch (error) {

                  }

                  try {
                      title = data.elements[i].currentPositions[0].title;
                  } catch (error) { }

                  try {
                      
                          let year = data.elements[i].currentPositions[0].startedOn.year;
                          let month = data.elements[i].currentPositions[0].startedOn.month;
                          var myDate = Math.floor(new Date(year,month).getTime()/1000.0);
                         // console.log(Date.now())
                          tenure = myDate;
                      
                      
                  } catch (error) {

                  }

                  try {
                      companyName = data.elements[i].currentPositions[0].companyName;
                  } catch (error) { }

                  // checking if the Profile has PastRole
                  try {
                      if (data.elements[i].pastPositions === undefined) {
                          past_role = [];
                      }
                      else {
                          past_role = data.elements[i].pastPositions;
                      }

                  } catch (error) { }


                  // checking if the profile has Industry information
                  try {

                      if (data.elements[i].currentPositions[0].companyUrnResolutionResult.industry === undefined) {
                          industry = "";
                      }
                      else {
                          industry = data.elements[i].currentPositions[0].companyUrnResolutionResult.industry;
                      }
                  } catch (error) { }


                  // Checking if the profilePicture is not throwing errror
                  try {
                      if (data.elements[i].profilePictureDisplayImage === undefined) {
                          profilePictureUrl = "";
                      }
                      else {
                          profilePictureUrl = data.elements[i].profilePictureDisplayImage.rootUrl + data.elements[i].profilePictureDisplayImage.artifacts[3].fileIdentifyingUrlPathSegment;
                      }
                  } catch (error) { }

                  // Making a string for the Profiles which are in your CONNECTION
                  try {
                      if (data.elements[i].degree == -1) {
                          conn_degree = "Your Connection";
                      }
                      else {
                          conn_degree = data.elements[i].degree;
                      }


                  } catch (error) { }

                  // Giving Premium a string YES/No for true?false
                  try {
                      if (data.elements[i].premium == undefined) {
                          premium = "";
                      }
                      else {
                          premium = data.elements[i].premium;
                      }
                  } catch (error) { }

                  try {
                    firstName = data.elements[i].firstName.replaceAll(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                    lastName = data.elements[i].lastName.split(" ")[0].replaceAll(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                  } catch (error) {
                      
                  }



                  // Pushing Data into Array Object named Data.Members
                  Data.members.push({
                      name: firstName + " " + lastName,
                      first_name: firstName ,
                      last_name: lastName ,
                      company: companyName,
                     // company_domain: "",
                      designation: title,
                      location: data.elements[i].geoRegion,
                      tenure_at_company: tenure,
                      premium_member: premium,
                      connection_degree: conn_degree,
                      industry: industry,
                      profile_viewed: visited,
                      company_linkedin_url: company_url,
                      profile_pic: profilePictureUrl,
                      linkedin_profile_link: profile_url,
                      about: summary,
                      experiences: past_role,

                  })

                  
                      /*
                  try {
                      
                      var myHeadersData = new Headers();
                      myHeadersData.append("cookie", cookie);
                      myHeadersData.append("csrf-token", csrf);
                      myHeadersData.append("x-restli-protocol-version", "2.0.0");

                      var requestOp = {
                          method: 'GET',
                          headers: myHeadersData,
                          redirect: 'follow'
                      };
                   
                      try {

                          if (companyNo === "") {
                              companyNo = 100;
                          }
                          else {

                              fetch(`https://www.linkedin.com/sales-api/salesApiCompanies/${companyNo}?decoration=%28website%29`, requestOptions)
                                  .then(response => response.text())
                                  .then(result => {
                                      obj = JSON.parse(result);
                                      //Data.members[i].companyURl = obj.website;

                                      //console.log(obj)

                                      try {
                                          if (obj.website === undefined) {
                                              company_domain_url = "Not Available";
                                              // console.log("Not Worked")

                                          }
                                          else {
                                              company_domain_url = obj.website;
                                              // console.log("worked")
                                          }
                                      } catch (error) {
                                      }
                                      Data.members[i].company_domain = company_domain_url;


                                  })
                                  .catch(error => console.log('error', error))
                          }

                      } catch (error) { } 
                      



                  } catch (error) { }

                  */


              }

             

          

          })
          .catch(error => console.log('error', error));
  }

  // startScrape();
  // console.log(Data)
/*    function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    sleep(15000);
    */

  for (let i = 1; i <= pages; i++) {

     // setTimeout(() => {
          
          startScrape();
          start += 100;
          if(extra !==0){
          if (i === pages - 1) {
              count = extra;
          }
      }

     // }, 14000 * i);

  }




  setTimeout(() => {
      Data.total_members_scraped = Data.members.length;
      console.log(Data)
  }, 3000);


  })

    

    function exportCSVFile(headers, items, fileTitle) {
      if (headers) {
          items.unshift(headers);
      }
  
      // Convert Object to JSON
      var file = Data;
  
      var csv = this.convertToCSV(file);
  
      var exportedFilenmae = fileTitle + ".csv" || "export.csv";
  
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      if (navigator.msSaveBlob) {
          // IE 10+
          navigator.msSaveBlob(blob, exportedFilenmae);
      } else {
          var link = document.createElement("a");
          if (link.download !== undefined) {
              // feature detection
              // Browsers that support HTML5 download attribute
              var url = URL.createObjectURL(blob);
              link.setAttribute("href", url);
              link.setAttribute("download", exportedFilenmae);
              link.style.visibility = "hidden";
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
          }
      }
  }

}